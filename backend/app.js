require("./settings");
const express = require("express");
const app = express();
const cors = require("cors");
const mysql = require('./helpers/mysql');
const mkdirp = require('mkdirp');
const config = require('./config');


const bodyParser = require("body-parser");

const userController = require("./controller/userController");
const fileController = require("./controller/fileController");
const productController = require("./controller/productController");
const adminController = require("./controller/adminController");
const tokenController = require("./controller/tokenController");
const bbaiController = require("./controller/bbaiController");


app.use(cors());

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
  extended: false
}));

// parse application/json
app.use(bodyParser.json());



app.use("/api/user/", userController);
app.use("/api/file/", fileController);
app.use("/api/product/", productController);
app.use("/api/admin/", adminController);
app.use("/api/bbai/", bbaiController);
app.use("/api/token/", tokenController);

app.get("/test", (req, res) => {
  res.json({
    data: "etho"
  });
});

async function initializeServer() {
  try {
    await mkdirp(await config.datadir());
    console.log('Directory Created');
    await mysql.createDBConnection()
  } catch (error) {
    console.log(error);
  }
}

initializeServer();

app.listen(3000, () => {
  console.log("Server running on port 3000");
});



module.exports.app = app;