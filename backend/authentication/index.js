require("../settings");
const jwt = require('jsonwebtoken');

module.exports = {

    signPayload: function (payload) {
        try {
            return  jwt.sign({
                user: payload
            }, process.env.jwtSecret);          
        } catch (error) {
            console.log(error);
            
        }
     
    },

    verifySignature: function (req, res, next) {

        try {
            
            const bearerHeader = req.headers['authorization'];

            if (typeof bearerHeader !== 'undefined') {

                const bearer = bearerHeader.split(' ');
                const bearerToken = bearer[1];
                req.token = bearerToken;
                next();

            } else {
                res.sendStatus(403);
            }

        } catch (error) {
            console.log(error);
            
        }

    },

    getPayload : function(token) {
        try {
            return jwt.verify(token, process.env.jwtSecret);
        } catch (error) {
            console.log(error)        
        }
    }


}