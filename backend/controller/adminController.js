const express = require("express");
const router = express.Router();
const userHelper = require('../helpers/user');
const auth = require('../authentication/index');
const createTokentxn = require("../transactions/createToken");
const tokenHelper = require("../helpers/token");
const wallet = require("../helpers/wallet");


router.post("/register", async (req, res) => {

    let data = req.body;
    console.log("called admin register api");
  
    try {
  
      if (data.password) {
        let isUserExisting = await userHelper.validateUserRegistration('admin');
  
        if (isUserExisting) {
          res.status(409).json({
            message: `admin account already exists`
          })
  
        } else {
          await userHelper.registerUser('admin', data.password);
          await userHelper.saveAdminPasswordSecret(data.password);
          res.status(200).json({
            message: `admin registered successfully`
          })
        }
  
      } else {
        res.status(400).json({
          message: `Body Should Contain Field "password" `
        })
      }
  
    } catch (error) {
  
      console.log(error);
      res.status(400).json({
        message: error
      })
    }
  
  });

  router.post("/login", async (req, res) => {

    let data = req.body;
    console.log("called admin login api");
  
    try {
  
      if (data.password) {
  
        let isUserExisting = await userHelper.validateUserRegistration('admin');

        console.log("here",isUserExisting);
  
        if (isUserExisting) {
  
          const {
            userLoginStatus,
            userDetails
          } = await userHelper.loginUser('admin', data.password);
  
          if (userLoginStatus) {
  
            userDetails.rawPassword = data.password;
  
            const token = auth.signPayload(userDetails);

  
            res.status(200).json({
              token: token
            })
  
  
          } else {
            res.status(400).json({
              message: `invalid password`
            })
  
          }
  
        } else {
  
          res.status(404).json({
            message: `admin does not exist`
          })
  
        }
  
      } else {
        res.status(400).json({
          message: `Body Should Contain Field "password" `
        })
      }
  
    } catch (error) {
  
      console.log(error);
      res.status(400).json({
        message: error
      })
  
    }
  
  });

  router.post("/createBBAI", auth.verifySignature, async (req, res) => {

    let data = req.body;
    data.tokenName = "bbai";
    data.tokenSymbol = "bbai";
    console.log("called BBAI Token Creator api");
  
    try {
      if (data.tokenSupply) {

          const userDetails = auth.getPayload(req.token)["user"];
          console.log(userDetails);
          if(userDetails.username == 'admin') {

            const {isBBAITokenAlreadyDeployed,bbaiTokenDetails} = await tokenHelper.getBBAITokenDetailsIfExist();

            if(isBBAITokenAlreadyDeployed) {

              res.status(400).json({
                message : `BBAI Token Already Deployed`,
                details: bbaiTokenDetails
              });

            } else {
           
            const { userLoginStatus } = await userHelper.loginUser(
              userDetails.username,
              userDetails.rawPassword
            );

            console.log("user login status",userLoginStatus)
    
            if (userLoginStatus) {
              const privateKey = await wallet.getPrivateKey(
                userDetails.address,
                userDetails.rawPassword
              );

              console.log("private key",privateKey);
    
              const {
                tokenCreatedStatus,
                tokenCreatedDetails
              } = await createTokentxn.create(
                privateKey,
                userDetails.address,
                data.tokenName,
                data.tokenSymbol,
                data.tokenSupply
              );
    
              console.log(tokenCreatedStatus, tokenCreatedDetails);
    
              if (tokenCreatedStatus) {
                await tokenHelper.saveTokenDetails(
                  tokenCreatedDetails.address,
                  data.tokenName,
                  data.tokenSymbol,
                  data.tokenSupply,
                  userDetails.address
                );
    
                res.status(200).json({
                  message: "Token Created",
                  address: tokenCreatedDetails.address
                });
    
      
              } else {
                res.status(400).json({
                  message: `error creating token`
                });
              }
            } else {
              res.status(400).json({
                message: `invalid password`
              });
            }
          }
          } else {
            res.status(400).json({
              message: `Forbidden! Only Admin Can Create BBAI Token`
            });
          }
      } else {
        res.status(400).json({
          message: `Body Should Contain Field "tokenSupply"`
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: error
      });
    }
  });




module.exports = router;