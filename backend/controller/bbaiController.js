const express = require("express");
const router = express.Router();
const auth = require("../authentication/index");
const tokenHelper = require("../helpers/token");
const tokenTransactions = require("../transactions/tokenFunctions");
const wallet = require("../helpers/wallet");
const userHelper = require('../helpers/user');

router.post("/buy", auth.verifySignature, async (req, res) => {

    let data = req.body;
    console.log("called token buy api");
  
    try {

      if (data.to && data.amount_of_bbai) {

  
        if (!isNaN(data.amount_of_bbai)) {
            //get bbai details
            const {isBBAITokenAlreadyDeployed,bbaiTokenDetails} = await tokenHelper.getBBAITokenDetailsIfExist()

            if(isBBAITokenAlreadyDeployed) {

                console.log(bbaiTokenDetails);

                const bbaiOwnerTokenBalance = await tokenTransactions.getBalanceOfUser(
                    bbaiTokenDetails[0].token_deployed_owner,
                    bbaiTokenDetails[0].token_deployed_address
                );

                console.log(bbaiOwnerTokenBalance,data.amount_of_bbai);

                if (bbaiOwnerTokenBalance >= data.amount_of_bbai) {
                    //owner has enough balance
                    console.log("bbai owner has enough balance");

                    const adminPassword = await userHelper.getAdminPassword();
               
                    const privateKey = await wallet.getPrivateKey(
                        bbaiTokenDetails[0].token_deployed_owner,
                        adminPassword
                        );
                    console.log("pvt key",privateKey);

                    const { txStatus, txDetails } = await tokenTransactions.transferCoin(
                        privateKey,
                        data.to,
                        data.amount_of_bbai,
                        bbaiTokenDetails[0].token_deployed_address
                      );
            
                      if (txStatus) {
                        res.status(200).json({
                          message: `success`,
                          transaction_details: txDetails
                        });
                      } else {
                        res.status(400).json({
                          message: `txn failed`
                        });
                      }


                } else {

                    res.status(400).json({
                        message: `BBAI doesnot have required token balance to transfer`
                        });

                }


            } else {
                res.status(400).json({
                    message: `BBAI Token Does Not Exist / Not Yet Created`
                  });
            }

        } else {
          res.status(400).json({
            message: `The specified amount_of_bbai is not a valid number`
          });
        }

      } else {
        res.status(400).json({
          message: `Body Should Contain Fields "to", "amount_of_bbai"`
        });
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        message: error
      });
    }
  });






module.exports = router;