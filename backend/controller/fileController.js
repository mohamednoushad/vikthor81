require("../settings");
const express = require("express");
const router = express.Router();
const auth = require('../authentication/index');
var multer = require('multer')
const inMemoryStorage = multer.memoryStorage();
const uploadStrategy = multer({
    storage: inMemoryStorage
}).single('image');
const containerName = 'images';
const getStream = require('into-stream');
const fileHelper = require("../helpers/file");


const azureStorage = require('azure-storage');

const blobService = azureStorage.createBlobService(process.env.blobName, process.env.blobKey);


const getBlobName = originalName => {
    const identifier = Math.random().toString().replace(/0\./, ''); // remove "0." from start of string
    return `${identifier}-${originalName}`;
};


router.post("/uploadImage", auth.verifySignature, uploadStrategy, async (req, res) => {

    const blobName = getBlobName(req.file.originalname);
    const stream = getStream(req.file.buffer);
    const streamLength = req.file.buffer.length;

    let data = req.body;

    const userDetails = auth.getPayload(req.token)["user"];

    try {

        if (data.productId && data.fileType) {

            let typeOfPhotosAllowed = [
                "main_photo_1",
                "main_photo_2",
                "main_photo_3",
                "damage_photo",
                "official_document",
                "guarantee_paper",
                "invoice",
                "passport"
            ]

            if (typeOfPhotosAllowed.includes(data.fileType)) {

                const isProductAlreadyPresent = await fileHelper.isProductAlreadyExisting(data.productId);

                blobService.createBlockBlobFromStream(containerName, blobName, stream, streamLength, err => {

                    if (err) {
                        console.log(err);
                        return;
                    }

                    let url = blobService.getUrl(containerName, blobName);


                    if (isProductAlreadyPresent) {
                        fileHelper.updateFileUrlToProduct(data.productId, data.fileType, url);
                    } else {
                        fileHelper.addFileUrlAndProduct(userDetails.address, data.productId, data.fileType, url);
                    }

                    res.status(200).json({
                        "response": {
                            "response_code": 200,
                            "token": null
                        },
                        "details": {
                            "result": "File Uploaded Successfully",
                            "url": url
                        }
                    })

                });

            } else {

                res.status(200).json({
                    "response": {
                        "response_code": 409,
                        "token": null
                    },
                    "details": {
                        "result": `The 'fileType' should be any of the following : ${typeOfPhotosAllowed} `
                    }
                })
            }

        } else {

            res.status(200).json({
                "response": {
                    "response_code": 409,
                    "token": null
                },
                "details": {
                    "result": "The body should contain 'productId' and 'fileType'"
                }
            })



        }


    } catch (error) {

        console.log("this error", error);

    }


});



module.exports = router;