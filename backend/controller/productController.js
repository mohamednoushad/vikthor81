require("../settings")
const express = require("express");
const router = express.Router();
const auth = require('../authentication/index');
const productHelper = require("../helpers/product");
const wallet = require("../helpers/wallet");
const createTokentxn = require("../transactions/createToken");
const ethTxns = require("../transactions/ethTransactions");
const txnHelper = require("../transactions/helper");


router.post("/add", auth.verifySignature, async (req, res) => {

    let data = req.body;

    const userDetails = auth.getPayload(req.token)["user"];

    console.log(userDetails);

    try {

        if (data.productId && data.name && data.description &&
            data.brand && data.serialno && data.size &&
            data.year_of_production && data.year_of_buying &&
            data.price_in_eth && data.owner_address_1 &&
            data.owner_address_2 && data.contact_number &&
            data.repaired_status && data.repaired_part &&
            data.repaired_date && data.preowner_status &&
            data.preowner_address1 && data.preowner_address2 &&
            data.preowner_contact_number && data.security_features_description &&
            data.place_of_buying) {

            const isProductAlreadyPresent = await productHelper.isProductAlreadyExisting(data.productId);


            if (isProductAlreadyPresent) {

                const userBalance = await ethTxns.getEthBalance(userDetails.address);

                console.log("this is user balance", userBalance);

                let balanceRequiredByUser = process.env.OwnerSetFee;

                if (+(userBalance) > 2 * +(balanceRequiredByUser)) {

                    const privateKey = await wallet.getPrivateKey(
                        userDetails.address,
                        userDetails.rawPassword
                    );

                    const {
                        tokenCreatedStatus,
                        tokenCreatedDetails
                    } = await createTokentxn.create(
                        privateKey,
                        userDetails.address,
                        "TRUST24",
                        "T24",
                        1
                    );

                    


                    if (tokenCreatedStatus) {

                        console.log("add product to db");

                        await productHelper.addProductDetails(data, tokenCreatedDetails.address);

                        // ethTxns.transferEth(privateKey, process.env.OwnerAddress, process.env.OwnerSetFee);

                        productHelper.addTxnHashForTokenCreated(data.productId, tokenCreatedDetails.address,tokenCreatedDetails.txnHash);

                


                        res.status(200).json({
                            "response": {
                                "response_code": 200,
                                "token": null
                            },
                            "details": {
                                "result": "Product Added Succesfully",
                                "address": userDetails.address,
                                "balance": +(userBalance),
                                "balance_required": 2 * +(balanceRequiredByUser),
                                "NFT_Address": tokenCreatedDetails.address,
                                "productId": data.productId
                            }
                        })


                    } else {

                        res.status(200).json({
                            "response": {
                                "response_code": 409,
                                "token": null
                            },
                            "details": {
                                "result": "Error Creating Token",
                                "address": userDetails.address,
                                "balance": +(userBalance),
                                "balance_required": 2 * +(balanceRequiredByUser),
                                "NFT_Address": null,
                                "productId": data.productId
                            }
                        })

                    }

                } else {
                    console.log("user does not have balance")
                    res.status(200).json({
                        "response": {
                            "response_code": 409,
                            "token": null
                        },
                        "details": {
                            "result": "User Does Not Have Balance. Recharge Your Address with Eth",
                            "address": userDetails.address,
                            "balance": +(userBalance),
                            "balance_required": 2 * +(balanceRequiredByUser),
                            "NFT_Address": null,
                            "productId": data.productId
                        }
                    })
                }

            } else {

                res.status(200).json({
                    "response": {
                        "response_code": 409,
                        "token": null
                    },
                    "details": {
                        "result": `You have not uploaded any photo for this product id.
                                Please upload atleast the main photo of the product`,
                        "address": null,
                        "balance": null,
                        "balance_required": null,
                        "NFT_Address": null,
                        "productId": null
                    }
                })

            }

        } else {

            res.status(200).json({
                "response": {
                    "response_code": 409,
                    "token": null
                },
                "details": {
                    "result": `The body should contain productId, name, description,
                    brand, serialno, size, year_of_production, year_of_buying, price_in_eth,
                    owner_address_1, owner_address_2, contact_number, repaired_status, repaired_part, repaired_date,preowner_status,preowner_address1,
                    preowner_address2, preowner_contact_number, security_features_description and place_of_buying`,
                    "address": null,
                    "balance": null,
                    "balance_required": null,
                    "NFT_Address": null,
                    "productId": null
                }
            })

        }


    } catch (error) {

        console.log("this error", error);

        res.status(200).json({
            "response": {
                "response_code": 409,
                "token": null
            },
            "details": {
                "result": error,
                "address": null,
                "balance": null,
                "balance_required": null,
                "NFT_Address": null,
                "productId": null
            }
        })

    }


});

router.get("/user", auth.verifySignature, async (req, res) => {

    const userDetails = auth.getPayload(req.token)["user"];

    try {

        const result = await productHelper.getProductsOfUser(userDetails.address);

        console.log("this is result", result);


        res.status(200).json({
            "response": {
                "response_code": 200,
                "token": null
            },
            "details": {
                result
            }
        })



    } catch (error) {

        console.log("this error", error);

        res.status(200).json({
            "response": {
                "response_code": 409,
                "token": null
            },
            "details": {
                "result": error
            }
        })


    }


});


router.get("/", auth.verifySignature, async (req, res) => {

    const data = req.query;

    try {

        if (data.productId) {

            const productResult = await productHelper.getProductDetailsFromProductId(data.productId);

            if (productResult) {

                res.status(200).json({
                    "response": {
                        "response_code": 200,
                        "token": null
                    },
                    "details": {
                        "result": productResult
                    }
                })



            } else {

                res.status(200).json({
                    "response": {
                        "response_code": 409,
                        "token": null
                    },
                    "details": {
                        "result": "No product found for the product id"
                    }
                })

            }



        } else {

            res.status(200).json({
                "response": {
                    "response_code": 409,
                    "token": null
                },
                "details": {
                    "result": "Please provide 'productId' as query parameter"
                }
            })

        }

    } catch (error) {

        console.log(error);

        res.status(200).json({
            "response": {
                "response_code": 409,
                "token": null
            },
            "details": {
                "result": error
            }
        })

    }
});

router.get("/history", auth.verifySignature, async (req, res) => {

    const data = req.query;

    try {
        if (data.productId) {

            const productResult = await productHelper.getProductHistoryFromProductId(data.productId);
            // console.log(productResult);
            const productDetailsFromBlockchain = await txnHelper.getBlockchainDetails(productResult[0]);
            console.log(productDetailsFromBlockchain);

          


            if (productResult) {

                res.status(200).json({
                    "response": {
                        "response_code": 200,
                        "token": null
                    },
                    "details": {
                        "result": productDetailsFromBlockchain
                    }
                })

            } else {

                res.status(200).json({
                    "response": {
                        "response_code": 409,
                        "token": null
                    },
                    "details": {
                        "result": "No product found for the product id"
                    }
                })

            }

        } else {

            res.status(200).json({
                "response": {
                    "response_code": 409,
                    "token": null
                },
                "details": {
                    "result": "Please provide 'productId' as query parameter"
                }
            })

        }


    } catch (error) {

        console.log("this error", error);

        res.status(200).json({
            "response": {
                "response_code": 409,
                "token": null
            },
            "details": {
                "result": error
            }
        })

    }

});



module.exports = router;