const express = require("express");
const router = express.Router();
const auth = require("../authentication/index");
const userHelper = require("../helpers/user");
const tokenHelper = require("../helpers/token");
const wallet = require("../helpers/wallet");
const createTokentxn = require("../transactions/createToken");
const tokenTransactions = require("../transactions/tokenFunctions");

router.post("/create", auth.verifySignature, async (req, res) => {
  let data = req.body;
  console.log("called token creator api");

  try {
    if (data.tokenName && data.tokenSymbol && data.tokenSupply) {

      if (data["tokenName"].toLowerCase() == "bbai" || data["tokenSymbol"].toLowerCase() == "bbai") {
        console.log("user trying to create bbai coin");
        res.status(400).json({
          message: `you cannot create BBAI token, create token with any other name`
        });
      } else {
        const userDetails = auth.getPayload(req.token)["user"];
        console.log(userDetails);
        const {
          userLoginStatus
        } = await userHelper.loginUser(
          userDetails.username,
          userDetails.rawPassword
        );

        if (userLoginStatus) {
          const privateKey = await wallet.getPrivateKey(
            userDetails.address,
            userDetails.rawPassword
          );

          const {
            tokenCreatedStatus,
            tokenCreatedDetails
          } = await createTokentxn.create(
            privateKey,
            userDetails.address,
            data.tokenName,
            data.tokenSymbol,
            data.tokenSupply
          );

          console.log(tokenCreatedStatus, tokenCreatedDetails);

          if (tokenCreatedStatus) {
            await tokenHelper.saveTokenDetails(
              tokenCreatedDetails.address,
              data.tokenName,
              data.tokenSymbol,
              data.tokenSupply,
              userDetails.address
            );

            res.status(200).json({
              message: "Token Created",
              address: tokenCreatedDetails.address
            });


          } else {
            res.status(400).json({
              message: `error creating token`
            });
          }
        } else {
          res.status(400).json({
            message: `invalid password`
          });
        }
      }
    } else {
      res.status(400).json({
        message: `Body Should Contain Fields "tokenName", "tokenSymbol", "tokenSupply" and "password" `
      });
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: error
    });
  }
});

router.post("/transfer", auth.verifySignature, async (req, res) => {
  let data = req.body;
  console.log("called token transfer api");

  try {
    if (data.to && data.amount_to_transfer && data.token_deployed_address) {
      const userDetails = auth.getPayload(req.token)["user"];
      console.log(userDetails);

      if (!isNaN(data.amount_to_transfer)) {
        //check if user has enough balance
        const userTokenBalance = await tokenTransactions.getBalanceOfUser(
          userDetails.address,
          data.token_deployed_address
        );

        console.log(userTokenBalance);
        console.log(userTokenBalance >= data.amount_to_transfer);

        if (+(userTokenBalance) >= +(data.amount_to_transfer)) {
          //transfer the coins
          const privateKey = await wallet.getPrivateKey(
            userDetails.address,
            userDetails.rawPassword
          );
          console.log(privateKey);

          const {
            txStatus,
            txDetails
          } = await tokenTransactions.transferCoin(
            privateKey,
            data.to,
            data.amount_to_transfer,
            data.token_deployed_address
          );

          if (txStatus) {
            await tokenHelper.saveTokenTransferInDB(txDetails.transactionHash, txDetails.from, txDetails.to, data.token_deployed_address, data.amount_to_transfer, data.description);
            res.status(200).json({
              message: `success`,
              transaction_details: txDetails
            });
          } else {
            res.status(400).json({
              message: `txn failed`
            });
          }
        } else {
          console.log("in else");
          res.status(400).json({
            message: `user doesnot have enough token balance to transfer`
          });
        }
      } else {
        res.status(400).json({
          message: `The specified transfer amount is not a valid number`
        });
      }

    } else {
      res.status(400).json({
        message: `Body Should Contain Fields "to", "amount_to_transfer" and "token_deployed_address" `
      });
    }
  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: error
    });
  }
});

router.get("/listAll", auth.verifySignature, async (req, res) => {

  console.log("called listing all tokens deployed api");

  try {

    const tokenDetails = await tokenHelper.getAllCreatedTokenDetails();

    if (tokenDetails) {
      res.status(200).json({
        tokenDetails
      });
    } else {
      res.status(400).json({
        message: `Oops, Something Went Wrong !`
      });
    }


  } catch (error) {
    console.log(error);
    res.status(400).json({
      message: error
    });
  }
});

module.exports = router;