const express = require("express");
const router = express.Router();
const userHelper = require('../helpers/user');
const auth = require('../authentication/index');
const ethTxns = require("../transactions/ethTransactions");


router.post("/register", async (req, res) => {

  let data = req.body;
  console.log("called register api");

  try {

    if (data.username && data.password) {

      if (data.username == "admin") {


        res.status(200).json({
          "response": {
            "response_code": 409,
            "token": null
          },
          "details": {
            "result": "You cannot register as admin",
            "username": null,
            "user_address": null,
            "ether_balance": null,
          }
        })

      } else {

        let isUserExisting = await userHelper.validateUserRegistration(data.username);

        if (isUserExisting) {

          res.status(200).json({
            "response": {
              "response_code": 409,
              "token": null
            },
            "details": {
              "result": "A User with Username is already Present",
              "username": null,
              "user_address": null,
              "ether_balance": null,
            }
          })

        } else {
          const userDetails = await userHelper.registerUser(data.username, data.password);
          console.log("this", userDetails);
          const token = auth.signPayload(userDetails);
          const userEthBalance = await userHelper.getUserEthBalance(userDetails.address);

          res.status(200).json({
            "response": {
              "response_code": 200,
              "token": token
            },
            "details": {
              "result": "User Registered Successfully",
              "username": data.username,
              "user_address": userDetails.address,
              "ether_balance": userEthBalance,
            }
          })

        }
      }

    } else {

      res.status(200).json({
        "response": {
          "response_code": 400,
          "token": null
        },
        "details": {
          "result": "Body Should Contain Fields 'username' and 'password'",
          "username": null,
          "user_address": null,
          "ether_balance": null,
        }
      })
    }

  } catch (error) {

    console.log(error);
    res.status(400).json({
      message: error
    })
  }

});

router.post("/login", async (req, res) => {

  let data = req.body;
  console.log("called login api");

  try {

    if (data.username && data.password) {

      let isUserExisting = await userHelper.validateUserRegistration(data.username);

      if (isUserExisting) {

        const {
          userLoginStatus,
          userDetails
        } = await userHelper.loginUser(data.username, data.password);

        if (userLoginStatus) {

          userDetails.rawPassword = data.password;

          console.log("this is user details", userDetails);

          const token = auth.signPayload(userDetails);

          const userEthBalance = await userHelper.getUserEthBalance(userDetails.address);


          res.status(200).json({
            "response": {
              "response_code": 200,
              "token": token
            },
            "details": {
              "result": "success",
              "username": data.username,
              "user_address": userDetails.address,
              "ether_balance": userEthBalance,
            }
          })


        } else {

          res.status(200).json({
            "response": {
              "response_code": 400,
              "token": null
            },
            "details": {
              "result": "Invalid Password",
              "username": null,
              "user_address": null,
              "ether_balance": null,
            }
          })

        }

      } else {


        res.status(200).json({
          "response": {
            "response_code": 404,
            "token": null
          },
          "details": {
            "result": "User Not Found",
            "username": null,
            "user_address": null,
            "ether_balance": null,
          }
        })

      }

    } else {

      res.status(200).json({
        "response": {
          "response_code": 400,
          "token": null
        },
        "details": {
          "result": "Body Should Contain Fields 'username' and 'password'",
          "username": null,
          "user_address": null,
          "ether_balance": null,
        }
      })
    }

  } catch (error) {

    console.log(error);

    res.status(200).json({
      "response": {
        "response_code": 400,
        "token": null
      },
      "details": {
        "result": error,
        "username": null,
        "user_address": null,
        "ether_balance": null,
      }
    })

  }

});

router.get("/balance", auth.verifySignature, async (req, res) => {

  try {

    const userDetails = auth.getPayload(req.token)["user"];

    const userBalance = await ethTxns.getEthBalance(userDetails.address);

    let balanceRequiredByUser = process.env.OwnerSetFee;

    res.status(200).json({
      "response": {
        "response_code": 200,
        "token": null
      },
      "details": {
        "result": "see balance key in the response",
        "address": userDetails.address,
        "balance": +(userBalance),
        "balance_required": 2 * +(balanceRequiredByUser)
      }
    })

  } catch (error) {

    res.status(200).json({
      "response": {
        "response_code": 409,
        "token": null
      },
      "details": {
        "result": error
      }
    })

  }

});



module.exports = router;