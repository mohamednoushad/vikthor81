-- MySQL dump 10.13  Distrib 5.7.31, for Linux (x86_64)
--
-- Host: localhost    Database: vikthor_middleware
-- ------------------------------------------------------
-- Server version	5.7.31-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `brand` varchar(45) DEFAULT NULL,
  `serialno` varchar(45) DEFAULT NULL,
  `size` varchar(45) DEFAULT NULL,
  `year_of_production` varchar(45) DEFAULT NULL,
  `year_of_buying` varchar(45) DEFAULT NULL,
  `price_in_eth` varchar(45) DEFAULT NULL,
  `main_photo_1` text,
  `main_photo_2` text,
  `main_photo_3` text,
  `damage_photo` text,
  `official_document` text,
  `guarantee_paper` text,
  `invoice` text,
  `passport` text,
  `owner_eth_address` varchar(45) DEFAULT NULL,
  `owner_address_1` varchar(45) DEFAULT NULL,
  `owner_address_2` varchar(45) DEFAULT NULL,
  `contact_number` varchar(45) DEFAULT NULL,
  `date_of_adding` varchar(45) DEFAULT NULL,
  `contract_address` varchar(45) DEFAULT NULL,
  `for_selling` int(11) DEFAULT '0',
  `transferred_status` int(11) DEFAULT '0',
  `bought_status` int(11) DEFAULT '0',
  `transferredOrBoughtOwner` varchar(45) DEFAULT NULL,
  `repaired_status` int(11) DEFAULT '0',
  `repaired_part` varchar(45) DEFAULT NULL,
  `repaired_date` varchar(45) DEFAULT NULL,
  `preowner_status` int(11) DEFAULT '0',
  `preowner_address1` varchar(45) DEFAULT NULL,
  `preowner_address2` varchar(45) DEFAULT NULL,
  `preowner_contact_number` varchar(45) DEFAULT NULL,
  `security_features_description` varchar(45) DEFAULT NULL,
  `place_of_buying` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `token_transfers`
--

DROP TABLE IF EXISTS `token_transfers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token_transfers` (
  `txn_hash` varchar(500) NOT NULL,
  `from_address` varchar(45) DEFAULT NULL,
  `to_address` varchar(45) DEFAULT NULL,
  `token_deployed_address` varchar(45) DEFAULT NULL,
  `amount_of_tokens` varchar(45) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`txn_hash`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transaction_details`
--

DROP TABLE IF EXISTS `transaction_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` varchar(45) DEFAULT NULL,
  `token_contract_address` varchar(45) DEFAULT NULL,
  `token_created_txn_hash` tinytext,
  `token_transferred_txn_hash` tinytext,
  `token_bought_txn_hash` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `username` varchar(45) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-10-03 14:39:49
