const DBService = require("./dbservice");


module.exports = {

    isProductAlreadyExisting: async function (productId) {

        try {

            let queryForProduct = "SELECT id FROM products WHERE product_id = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForProduct,
                values: [productId]
            });

            console.log(resultOnQuery);

            if (resultOnQuery.length) {
                return true;
            } else {
                return false;
            }

        } catch (error) {

            console.log(error);

        }


    },



    addFileUrlAndProduct: async function (userAddress, productId, fileType, url) {

        let fileColumn;

        switch (fileType) {
            case "main_photo_1":
                fileColumn = "main_photo_1";
                break;
            case "main_photo_2":
                fileColumn = "main_photo_2";
                break;
            case "main_photo_3":
                fileColumn = "main_photo_3";
                break;
            case "damage_photo":
                fileColumn = "damage_photo";
                break;
            case "official_document":
                fileColumn = "official_document";
                break;
            case "guarantee_paper":
                fileColumn = "guarantee_paper";
                break;
            case "invoice":
                fileColumn = "invoice";
                break;
            case "passport":
                fileColumn = "passport";
                break;
        }

        try {

            var data = {
                table: "products",
                columns: ["product_id", fileColumn, "owner_eth_address"],
                values: [
                    [productId, url, userAddress]
                ]
            }

            console.log(data);

            await DBService.insertQuery(data);



        } catch (error) {

            console.log(error);


        }

    },

    updateFileUrlToProduct: async function (productId, fileType, url) {

        let fileColumn;

        switch (fileType) {
            case "main_photo_1":
                fileColumn = "main_photo_1";
                break;
            case "main_photo_2":
                fileColumn = "main_photo_2";
                break;
            case "main_photo_3":
                fileColumn = "main_photo_3";
                break;
            case "damage_photo":
                fileColumn = "damage_photo";
                break;
            case "official_document":
                fileColumn = "official_document";
                break;
            case "guarantee_paper":
                fileColumn = "guarantee_paper";
                break;
            case "invoice":
                fileColumn = "invoice";
                break;
            case "passport":
                fileColumn = "passport";
                break;
        }

        try {

            let queryForUpdate = `UPDATE products SET ${fileColumn} = ? WHERE product_id=?`;
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUpdate,
                values: [url, productId]
            });

   

        } catch (error) {

            console.log(error);

        }



    }

}