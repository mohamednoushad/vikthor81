const DBService = require("./dbservice");

function getTodaysDate() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    today = dd + '/' + mm + '/' + yyyy;

    return today;
}

module.exports = {

    isProductAlreadyExisting: async function (productId) {

        try {

            let queryForProduct = "SELECT id FROM products WHERE product_id = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForProduct,
                values: [productId]
            });

            console.log(resultOnQuery);

            if (resultOnQuery.length) {
                return true;
            } else {
                return false;
            }

        } catch (error) {

            console.log(error);

        }


    },

    addProductDetails: async function (data, tokenDeployedAddress) {

        try {

            let queryForUpdate = `UPDATE products SET    
            name = ? ,
            description = ? ,
            brand = ? ,
            serialno = ? ,
            size = ? ,
            year_of_production = ? ,
            year_of_buying = ? ,
            price_in_eth = ? ,
            owner_address_1 = ? ,
            owner_address_2 = ? ,
            contact_number = ? ,
            date_of_adding = ? ,
            contract_address = ? ,
            for_selling = ?  ,
            repaired_status = ? ,
            repaired_part = ? ,
            repaired_date = ? ,
            preowner_status = ? ,
            preowner_address1 = ? ,
            preowner_address2 = ? ,
            preowner_contact_number = ? ,
            security_features_description = ? ,
            place_of_buying = ? 
            WHERE product_id=?`;


            let resultOnQuery = await DBService.executeQuery({
                query: queryForUpdate,
                values: [data.name, data.description, data.brand, data.serialno, data.size,
                    data.year_of_production, data.year_of_buying, data.price_in_eth,
                    data.owner_address_1, data.owner_address_2, data.contact_number, getTodaysDate(),
                    tokenDeployedAddress, 1,
                    data.repaired_status, data.repaired_part, data.repaired_date, data.preowner_status,
                    data.preowner_address1, data.preowner_address2, data.preowner_contact_number, data.security_features_description,
                    data.place_of_buying, data.productId
                ]
            });

            console.log(resultOnQuery);



        } catch (error) {

            console.log(error);

        }

    },

    getProductsOfUser: async function (userAddress) {

        try {

            let queryForProductAddedOrTransferred = "SELECT product_id, name, main_photo_1, price_in_eth,contract_address,for_selling,transferred_status, bought_status FROM products WHERE owner_eth_address = ?";
            let resultOnQueryAddedOrTransferred = await DBService.executeQuery({
                query: queryForProductAddedOrTransferred,
                values: [userAddress]
            });

            console.log("see this", resultOnQueryAddedOrTransferred);

            let added = [];
            let transferred = [];
            let bought = [];

            if (resultOnQueryAddedOrTransferred.length) {

                resultOnQueryAddedOrTransferred.forEach(element => {

                    // console.log(element);

                    if (element.for_selling == 1 && element.transferred_status == 0 && element.bought_status == 0) {

                        added.push(element);

                    } else if (element.for_selling == 0 && element.transferred_status == 1 && element.bought_status == 0) {

                        transferred.push(element);


                    }

                });



            }

            let queryForProductBought = "SELECT product_id, name, main_photo_1, price_in_eth,contract_address,for_selling,transferred_status, bought_status FROM products WHERE transferredOrBoughtOwner = ?";
            let resultOnQueryBought = await DBService.executeQuery({
                query: queryForProductBought,
                values: [userAddress]
            });


            if (resultOnQueryBought.length) {

                resultOnQueryBought.forEach(element => {

                    if (element.for_selling == 0 && element.transferred_status == 1 || element.bought_status == 1) {

                        bought.push(element);

                    }

                })
            }

            return {
                products: {
                    "added": added,
                    "transferred": transferred,
                    "bought": bought
                }
            }


        } catch (error) {

            console.log(error);

        }

    },

    getProductDetailsFromProductId: async function (productId) {

        try {

            let queryForProduct = "SELECT * FROM products WHERE product_id = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForProduct,
                values: [productId]
            });

            if (resultOnQuery.length) {
                return resultOnQuery;
            } else {
                return false;
            }

        } catch (error) {

            console.log(error);

        }

    },

    addTxnHashForTokenCreated: async function (productId, contractAddress, createTokenTxnHash) {

        try {

            var data = {
                table: "transaction_details",
                columns: ["product_id", "token_contract_address", "token_created_txn_hash"],
                values: [
                    [productId, contractAddress, createTokenTxnHash]
                ]
            }

            console.log(data);

            await DBService.insertQuery(data);


        } catch (error) {

            console.log(error);


        }


    },

    getProductHistoryFromProductId: async function (productId) {

        try {

            let queryForProduct = "SELECT * FROM transaction_details WHERE product_id = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForProduct,
                values: [productId]
            });

            if (resultOnQuery.length) {
                return resultOnQuery;
            } else {
                return false;
            }

        } catch (error) {

            console.log(error);

        }

    }


}