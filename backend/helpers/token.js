const DBService = require("./dbservice");

module.exports = {
  saveTokenDetails: async function(
    tokenAddress,
    tokenName,
    tokenSymbol,
    tokenSupply,
    userAddress
  ) {
    let data = {
      table: "tokens",
      columns: [
        "token_deployed_address",
        "token_name",
        "token_symbol",
        "token_supply",
        "token_deployed_owner"
      ],
      values: [[tokenAddress, tokenName, tokenSymbol, tokenSupply, userAddress]]
    };

    try {
      await DBService.insertQuery(data);
    } catch (error) {
      console.log(error);
    }
  },

  getBBAITokenDetailsIfExist: async function() {
    try {
      let queryForUser =
        "SELECT * FROM tokens WHERE token_symbol = ? OR token_name = ?";
      let resultOnQuery = await DBService.executeQuery({
        query: queryForUser,
        values: ["bbai", "bbai"]
      });

      if (resultOnQuery.length) {
        return {
          isBBAITokenAlreadyDeployed: true,
          bbaiTokenDetails: resultOnQuery
        };
      } else {
        return {
          isBBAITokenAlreadyDeployed: false,
          bbaiTokenDetails: null
        };
      }
    } catch (error) {
      console.log(error);
    }
  },

  getAllCreatedTokenDetails: async function() {
    try {
      let queryForCoins = "SELECT * FROM tokens";
      let resultOnQuery = await DBService.executeQuery({
        query: queryForCoins,
        values: []
      });

      if (resultOnQuery.length) {
        return resultOnQuery;
      } else {
        return null;
      }
    } catch (error) {
      return error;
    }
  },

  saveTokenTransferInDB: async function(
    txnHash,
    fromAddress,
    toAddress,
    token_deployed_address,
    amount_of_tokens,
    description
  ) {
    let data = {
      table: "token_transfers",
      columns: [
        "txn_hash",
        "from_address",
        "to_address",
        "token_deployed_address",
        "amount_of_tokens",
        "description"
      ],
      values: [
        [
          txnHash,
          fromAddress,
          toAddress,
          token_deployed_address,
          amount_of_tokens,
          description
        ]
      ]
    };

    try {
      await DBService.insertQuery(data);
    } catch (error) {
      console.log(error);
    }
  }
};
