const DBService = require("./dbservice");
const walletService = require("./wallet");
const passwordHash = require("password-hash");
const tokenTransactions = require("../transactions/tokenFunctions");
const ethTransactions = require("../transactions/ethTransactions");
require("../settings");
// const AES = require("crypto-js/aes");
const CryptoJS = require("crypto-js");



module.exports = {

    validateUserRegistration: async function (username) {

        try {
            let queryForUser = "SELECT username FROM users WHERE username = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUser,
                values: [username]
            });

            if (resultOnQuery.length) {
                return true;
            } else {
                return false;
            }
        } catch (err) {
            throw err;
        }
    },

    registerUser: async function (username, password) {

        try {

            const keyObject = await walletService.generateKeyObject(password);
            let addressGeneratedForUser = "0x" + keyObject.address;
            await walletService.exportToFile(keyObject);


            const hashedPassword = passwordHash.generate(password);

            var data = {
                table: "users",
                columns: ["username", "password", "address"],
                values: [
                    [username, hashedPassword, addressGeneratedForUser]
                ]
            }

            await DBService.insertQuery(data);

            return {
                username : username,
                password: hashedPassword,
                address: addressGeneratedForUser,
                rawPassword: password
            }

        } catch (error) {

            console.log(error);

        }

    },

    loginUser: async function (username, password) {

        try {

            let queryForUser = "SELECT * FROM users WHERE username = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUser,
                values: [username]
            });

            if (resultOnQuery.length) {
                let userPasswordFromDb = resultOnQuery[0].password;

                const passWordCheck = passwordHash.verify(password, userPasswordFromDb)

                return {
                    userLoginStatus: passWordCheck,
                    userDetails: resultOnQuery[0]
                };

            } else {
                console.log("no user in db");
                return {
                    userLoginStatus: passWordCheck,
                    userDetails: null
                }
            }

        } catch (error) {

            console.log(error);

        }

    },

    getFullCoinsOfUser: async function (userAddress) {

        try {
            let queryForUser = "SELECT * FROM tokens WHERE token_deployed_owner = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUser,
                values: [userAddress]
            });

            if (resultOnQuery.length) {
                return {
                    userHasCoins: true,
                    userCoinDetails: resultOnQuery
                }
            } else {
                return {
                    userHasCoins: false,
                    userCoinDetails: null
                }
            }

        } catch (error) {

        }

    },

    getCoinsOfUserWithName: async function (userAddress, tokenName) {

        try {
            let queryForUser = "SELECT * FROM tokens WHERE token_deployed_owner = ? AND token_name = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUser,
                values: [userAddress, tokenName]
            });

            if (resultOnQuery.length) {
                return {
                    userHasCoins: true,
                    userCoinDetails: resultOnQuery
                }
            } else {
                return {
                    userHasCoins: false,
                    userCoinDetails: null
                }
            }

        } catch (error) {

        }

    },

    getTokenDetailsFromDeployedAddress: async function (tokenAddress) {

        try {
            let queryForUser = "SELECT * FROM tokens WHERE token_deployed_address = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUser,
                values: [tokenAddress]
            });

            if (resultOnQuery.length) {
                return {
                    isTokenPresent: true,
                    tokenDetails: resultOnQuery
                }
            } else {
                return {
                    isTokenPresent: false,
                    tokenDetails: null
                }
            }

        } catch (error) {

            console.log(error);

        }

    },

    addTokenToUserWallet: async function (userAddress, tokenDetails) {

        try {

            var data = {
                table: "wallet",
                columns: ["user_address", "token_deployed_address", "token_name", "token_symbol"],
                values: [
                    [userAddress, tokenDetails[0].token_deployed_address, tokenDetails[0].token_name, tokenDetails[0].token_symbol]
                ]
            }

            try {
                await DBService.insertQuery(data);
                return true;

            } catch (error) {

                if (error.sqlState == 23000) {
                    return false;
                } else {
                    console.log(error);
                }


            }




        } catch (error) {

            console.log(error);

        }

    },

    getFullTokensInUserWallet: async function (userAddress) {

        try {
            let queryForUser = "SELECT * FROM wallet WHERE user_address = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUser,
                values: [userAddress]
            });

            if (resultOnQuery.length) {
                return {
                    userHasTokensInWallet: true,
                    userTokenDetails: resultOnQuery
                }
            } else {
                return {
                    userHasTokensInWallet: false,
                    userTokenDetails: null
                }
            }

        } catch (error) {

            console.log(error);

        }

    },

    getCoinsOfUserWithNameFromWallet: async function (userAddress, tokenName) {

        try {
            let queryForUser = "SELECT * FROM wallet WHERE user_address = ? AND token_name = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUser,
                values: [userAddress, tokenName]
            });

            if (resultOnQuery.length) {
                return {
                    userHasTokensInWallet: true,
                    userTokenDetails: resultOnQuery
                }
            } else {
                return {
                    userHasTokensInWallet: false,
                    userTokenDetails: null
                }
            }

        } catch (error) {

            console.log(error);

        }

    },

    addBalanceOfEachToken: async function (userAddress, allTokenDetails) {

        for (const i in allTokenDetails) {

            var tokenBalanceOfUser = await tokenTransactions.getBalanceOfUser(userAddress, allTokenDetails[i].token_deployed_address);
            console.log("balance of user", tokenBalanceOfUser);
            allTokenDetails[i].token_balance = tokenBalanceOfUser;
        }

    },

    saveAdminPasswordSecret: async function (password) {

        try {

            let queryForUser = "SELECT * FROM secret WHERE user = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUser,
                values: ["admin"]
            });

            console.log(resultOnQuery);

            console.log(password, process.env.adminSecretKey);

            var secret = CryptoJS.AES.encrypt(password, process.env.adminSecretKey).toString();
            console.log("this is secret", secret);

            if (resultOnQuery.length) {
                //update
                let queryForUser = "UPDATE secret SET secret = ? WHERE user=?";
                let resultOnQuery = await DBService.executeQuery({
                    query: queryForUser,
                    values: [secret,"admin"]
                });

                console.log("updated secret");

            } else {
                //insert
                var data = {
                    table: "secret",
                    columns: ["user", "secret"],
                    values: [
                        ["admin", secret]
                    ]
                }

                await DBService.insertQuery(data);

            }

        } catch (error) {
            console.log(error);
        }
    },

    getAdminPassword : async function() {

        try {

            let queryForUser = "SELECT * FROM secret WHERE user = ?";
            let resultOnQuery = await DBService.executeQuery({
                query: queryForUser,
                values: ["admin"]
            });

            console.log(resultOnQuery);

            if (resultOnQuery.length) {

                let encrypedSecret = resultOnQuery[0].secret;
                var bytes  = CryptoJS.AES.decrypt(encrypedSecret, process.env.adminSecretKey);
                var decryptedPassword = bytes.toString(CryptoJS.enc.Utf8);
                return decryptedPassword;

            } else {
                console.log("admin secret does not exist");
            }
            
        } catch (error) {
            console.log(error);
            
        }
    },

    getUserEthBalance : async function(address) {
        return await ethTransactions.getEthBalance(address);
    }


};