const keythereum = require("keythereum");
const promisefs = require('promise-fs');
const config = require('../config');



function importFromFile(address, data) {
    try {
        return keythereum.importFromFile(address, data);
    } catch (e) {
        console.log(e);
        throw e;
    }

}

function recover(password, keyObject) {
    return keythereum.recover(password, keyObject);
}

function exportToFile(keyObject, datadir) {
    return keythereum.exportToFile(keyObject, datadir);
}

module.exports = {
    makeKeyStoreDirectoryIsNotPresent: function (directory) {
        console.log(directory);
        mkdirp(directory, function (err) {
            if (err) {
                console.error(err);
                res.status(500).send(err);
            } else {
                console.log('KeyStore Directory Created');
            }
        });
    },
    generateKeyObject: function (password) {
        //Creating keystore
        var params = {
            keyBytes: 32,
            ivBytes: 16
        };
        var dk = keythereum.create(params);
        var options = {
            kdf: "pbkdf2",
            cipher: "aes-128-ctr",
            kdfparams: {
                c: 262144,
                dklen: 32,
                prf: "hmac-sha256"
            }
        };
        return keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);
    },
    exportToFile: async (keyObject) => exportToFile(keyObject, await config.datadir()),
    recover: (password, keyObject) => recover(password, keyObject),
    importFromFile: (address, data) => importFromFile(address, data),
    getPrivateKey: async (publicKey, password) => {
        console.log(publicKey)
        let datadir = await config.datadir();
        const keystoreFiles = await promisefs.readdir(datadir);
        if (keystoreFiles) {
            const keyObject = importFromFile(publicKey, datadir.replace("/keystore", ""));
            const keyObject_privateKey = recover(password, keyObject);
            const privateKey = "0x" + keyObject_privateKey.toString('hex');
            return privateKey;
        } else {
            return {
                error: 404,
                message: "Keystore file unavailable"
            }
        }
    }
}