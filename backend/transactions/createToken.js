require("../settings")
const config = require("../config");

const {
    ethers
} = require("ethers");

const provider = new ethers.providers.JsonRpcProvider(process.env.providerUrl)

module.exports = {

    create: async function (pvtKey, accountAddress, tokenName, tokenSymbol, tokenSupply) {

        console.log("called create token transaction");

        const wallet = new ethers.Wallet(pvtKey, provider);

        console.log("1");

        const factory = new ethers.ContractFactory(config.tokenCreationContract.abi, config.tokenCreationContract.bytecode, wallet);

        console.log("2");

        try {

            const overrides = {
                gasLimit: 2000000,
                gasPrice: ethers.utils.parseUnits('100', 'gwei'),
                chainId: 4
            };

            console.log("3");

            let contract = await factory.deploy(tokenName, tokenSymbol, accountAddress, tokenSupply, overrides);

            console.log("contract address", contract.address);

            console.log("contract deployed transaction hash", contract.deployTransaction.hash);

            await contract.deployed()

            console.log("contract deployed");

            return {
                tokenCreatedStatus: true,
                tokenCreatedDetails: {
                    address: contract.address,
                    txnHash: contract.deployTransaction.hash
                }
            };

        } catch (error) {

            console.log(error);

            return {
                tokenCreatedStatus: false,
                tokenCreatedDetails: {
                    address: null
                }
            };

        }

    }

}