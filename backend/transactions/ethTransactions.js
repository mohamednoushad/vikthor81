require("../settings");

const {
    ethers
} = require("ethers");

const provider = new ethers.providers.JsonRpcProvider(process.env.providerUrl);


module.exports = {

    getEthBalance: async function (address) {

        const balance = await provider.getBalance(address);

        return ethers.utils.formatEther(balance);

    },

    transferEth: async function (pvtKey, toAddress, ethValue) {

        console.log("calling transfer eth");

        const wallet = new ethers.Wallet(pvtKey, provider);

        let tx = {
            to: toAddress,
            value: ethers.utils.parseEther(ethValue)
        };

        try {

            const sendPromise = await wallet.sendTransaction(tx);

            console.log("waiting for transfer to platform owner");

            console.log(sendPromise);

            // await provider.waitForTransaction(sendPromise.hash);

            // console.log("done transaction");

            return true;

        } catch (error) {

            console.log(error);

            return false;

        }


    }



}