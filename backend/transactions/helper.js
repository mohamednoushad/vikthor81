require("../settings");

const {
    ethers
} = require("ethers");

const provider = new ethers.providers.JsonRpcProvider(process.env.providerUrl);


async function getDetailsFromTxnHash(txnHash) {

    console.log(txnHash);
    const receipt = await provider.getTransactionReceipt(txnHash);
    console.log(receipt);

    var response = {
        from : receipt.from,
        txnHash: receipt.transactionHash,
        blockNumber: receipt.blockNumber,
        txnStatus: Boolean(receipt.status)
    }

    if(receipt.status) {
        response.txnStatus = "Success";
    } else {
        response.txnStatus = "Fail";
    }

    return response;

    // return {
    //     from : receipt.from,
    //     txnHash: receipt.transactionHash,
    //     blockNumber: receipt.blockNumber,
    //     txnStatus: Boolean(receipt.status)
    // }



}



module.exports = {

    getBlockchainDetails: async function (data) {

        console.log("calling this");

        console.log(data);

        let tokenCreatedTxnHash = data.token_created_txn_hash;
        let tokenTransferredTxnHash = data.token_transferred_txn_hash;
        let tokenBoughtTransactionHash = data.token_bought_txn_hash;

        var tokenCreatedDetails = await getDetailsFromTxnHash(tokenCreatedTxnHash);
        console.log(tokenCreatedDetails);

        console.log(tokenTransferredTxnHash)
        if(tokenTransferredTxnHash) {
            console.log("transferred status present");
            var tokenTransferredDetails = await getDetailsFromTxnHash(tokenTransferredTxnHash);
        }

        if(tokenBoughtTransactionHash) {
            console.log("transferred bought present");
            var tokenBoughtDetails = await getDetailsFromTxnHash(tokenBoughtTransactionHash);
        }

        return {
            created:tokenCreatedDetails,
            transferred:tokenTransferredDetails ? tokenTransferredDetails : null,
            bought:tokenBoughtDetails ? tokenBoughtDetails: null
        }






    }






}