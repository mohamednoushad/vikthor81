require("../settings")
const config = require("../config");

const Web3 = require('web3');
const web3 = new Web3(new Web3.providers.HttpProvider(process.env.providerUrl));

const {
    ethers
} = require("ethers");
const provider = new ethers.providers.JsonRpcProvider(process.env.providerUrl)

module.exports = {


    getBalanceOfUser: async function (userAddress, tokenDeployedAddress) {

        try {

            const tokenContract = new web3.eth.Contract(config.tokenCreationContract.abi, tokenDeployedAddress);
            const value = await tokenContract.methods.balanceOf(userAddress).call();
            return web3.utils.fromWei(value); //18 decimal units

        } catch (error) {

            console.log(error);

        }

    },

    transferCoin: async function (privateKey, toAddress, amountOfTokens, tokenDeployedAddress) {

        try {

            let overrides = {
                gasPrice: 0,
                gasLimit: 210000000
            };


            let wallet = new ethers.Wallet(privateKey, provider);

            const tokenContract = new ethers.Contract(tokenDeployedAddress, config.tokenCreationContract.abi, wallet);

            var amount = ethers.utils.parseEther(amountOfTokens.toString());

            const transaction = await tokenContract.transfer(toAddress, amount, overrides);

            console.log(transaction);

            const receipt = await provider.waitForTransaction(transaction.hash);

            console.log('receipt', receipt);

            if (receipt.status) {
                return {
                    txStatus: true,
                    txDetails: receipt
                }
            } else {
                return {
                    txStatus: false,
                    txDetails: null
                }
            }

        } catch (error) {

            console.log(error);

        }

    }

}